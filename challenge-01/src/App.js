
import './App.css';
import TextStyles from './components/textStyles';

function App() {
  let style =
    [
      { color: '#6A5ACD' }, { color: '#E0FFFF' },
      { color: '#00BFFF' }, { color: '#00FA9A' },
      { color: '#DAA520' }, { color: '#4B0082' },
      { color: '#FF00FF' }, { color: '#DC143C' },
      { color: '#FFFF00' }, { color: '#D5BFD8' },
    ]
  return style.map((style) => <TextStyles text="Hello Word" styles={style} />)
}

export default App;
